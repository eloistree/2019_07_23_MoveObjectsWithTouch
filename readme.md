# How to use: Move Objects With Touch   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.moveobjectswithtouch":"",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.moveobjectswithtouch",                              
  "displayName": "Move Objects With Touch",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Tools to move objects around with Oculus Touch",                         
  "keywords": ["Script","Tool"],                       
  "category": "Script",                   
  "dependencies":{}     
  }                                                                                
```    