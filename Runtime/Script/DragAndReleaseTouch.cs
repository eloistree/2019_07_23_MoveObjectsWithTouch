﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DragAndReleaseTouch : MonoBehaviour
{

    public Transform m_refPoint;
    public Transform m_trackedObject;
    public OnReleaseTransition m_onDragging;
    public OnReleaseTransition m_onRelease;

    public bool m_isDragging;
    public Vector3 m_startPosition;
    public Quaternion m_startRotation;
    public Vector3 m_currentPosition;
    public Quaternion m_currentRotation;
    public Vector3 m_cirrentEulerRotation;



    protected void StartDragging()
    {
        m_startPosition = m_currentPosition = GetLocalPositionOfHand();
        m_startRotation = m_currentRotation = GetLocalRotationOfHand();
        m_isDragging = true;
    }

    private Quaternion GetLocalRotationOfHand()
    {

        Quaternion localRotation = m_trackedObject.rotation * Quaternion.Inverse(m_refPoint.rotation);
        return localRotation;
    }

    private Vector3 GetLocalPositionOfHand()
    {
        Vector3 localPosition = m_refPoint.InverseTransformPoint(m_trackedObject.position);
        return localPosition;
    }
    protected void Dragging()
    {
        if (!m_isDragging)
            return;
        m_currentPosition = GetLocalPositionOfHand();
        m_currentRotation = GetLocalRotationOfHand();
        m_onDragging.Invoke(new ReleaseTransition() { m_localMove = GetTransitionVector(), m_localRotation = GetTransitionRotation() });
    }
    protected void EndDragging()
    {
        if (!m_isDragging)
            return;
        m_currentPosition = GetLocalPositionOfHand();
        m_currentRotation = GetLocalRotationOfHand();
        m_cirrentEulerRotation = m_currentRotation.eulerAngles ;
        m_onRelease.Invoke(new ReleaseTransition() { m_localMove = GetTransitionVector(), m_localRotation = GetTransitionRotation() });
        m_isDragging = false;
    }

    private Vector3 GetTransitionVector()
    {
        return m_currentPosition - m_startPosition;
    }

    private Quaternion GetTransitionRotation()
    {
        return m_currentRotation * Quaternion.Inverse(m_startRotation);
    }
}
[System.Serializable]
public class ReleaseTransition {
    public Vector3 m_localMove;
    public Quaternion m_localRotation;
}
[System.Serializable]
public class OnReleaseTransition : UnityEvent<ReleaseTransition> { }
