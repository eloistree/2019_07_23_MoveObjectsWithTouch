﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTargetWithDragAndRelease : MonoBehaviour
{
    public Transform m_object;
    public Transform m_ghost;
    public bool m_onlyVerticalRotation;
    public Vector3 m_euler;
    public float m_distanceMultiplicator = 1;
    public float m_rotationMultiplicator = 1;


    public void MoveGhost(ReleaseTransition transition)
    {
        if (m_ghost == null)
            return;
        Quaternion r = transition.m_localRotation;
        if (m_onlyVerticalRotation)
        {
            Vector3 e = r.eulerAngles;
            e.x = 0; e.z = 0;
            r = Quaternion.Euler(e);
        }

        Vector3 ee = r.eulerAngles;
        ee.x *= m_rotationMultiplicator;
        ee.y *= m_rotationMultiplicator;
        r = Quaternion.Euler(ee);

        m_ghost.gameObject.SetActive(true);
        m_ghost.position = m_object.position + m_object.rotation * (transition.m_localMove * m_distanceMultiplicator* m_distanceMultiplicator);
        m_ghost.rotation = r * m_object.rotation ;
        

    }

    public void Move(ReleaseTransition transition)
    {
        if (m_ghost != null)
            m_ghost.gameObject.SetActive(false);
        if (m_object == null)
            return;
        Quaternion r = transition.m_localRotation;
        if (m_onlyVerticalRotation)
        {
            Vector3 e = r.eulerAngles;
            e.x = 0; e.z = 0;
            r = Quaternion.Euler(e);
        }
        Vector3 ee = r.eulerAngles;
        ee.x *= m_rotationMultiplicator;
        ee.y *= m_rotationMultiplicator;
        r = Quaternion.Euler(ee);

        m_object.position = m_object.position +  m_object.rotation* (transition.m_localMove * m_distanceMultiplicator * m_distanceMultiplicator);
        m_object.rotation = r * m_object.rotation ;

    }
    
}
