﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragAndReleaseTouchWithTouchInput : DragAndReleaseTouch
{

    public OVRInput.Button m_button;

  
    void Update()
    {
        if (OVRInput.GetDown(m_button))
        {
            base.StartDragging();
        }
        if (OVRInput.Get(m_button))
        {
            base.Dragging();
        }
        if (OVRInput.GetUp(m_button))
        {
            base.EndDragging();
        }

    }
}
